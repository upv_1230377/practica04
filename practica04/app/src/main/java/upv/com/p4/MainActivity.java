package upv.com.p4;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.upv.myappliction.MESSAGE";
    private TextView tvResultado,tvResultado2,tvResultado3;

    private EditText mlado,mgrados,mmillas;
    private TextView res,resgrados,resmillas;
    private Button button,btnarea,btngrados,btnmillas;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("Tab 1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Tab 1");

        TabHost.TabSpec spec2 = tabHost.newTabSpec("Tab 2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Tab 2");

        TabHost.TabSpec spec3 = tabHost.newTabSpec("Tab 3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Tab 3");

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

        addListenerOnButton();




    }
    private void addListenerOnButton() {
        // TODO Auto-generated method stub

        mlado =(EditText) findViewById(R.id.etlado);
        btnarea  = (Button) findViewById(R.id.btnarea);
        res =(TextView) findViewById(R.id.resarea);
        tvResultado=(TextView) findViewById(R.id.resultadotab1);


        mgrados =(EditText) findViewById(R.id.etgrados);
        btngrados  = (Button) findViewById(R.id.btngrados);
        resgrados =(TextView) findViewById(R.id.resgrados);
        tvResultado2=(TextView) findViewById(R.id.resultadotab2);

        mmillas =(EditText) findViewById(R.id.etmillas);
        btnmillas  = (Button) findViewById(R.id.btnmillas);
        resmillas =(TextView) findViewById(R.id.resmillas);
        tvResultado3=(TextView) findViewById(R.id.resultadotab3);


        btnarea.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                double x = Double.valueOf(mlado.getText().toString());
                double resultado = x * x;
                String resultadofinal = resultado + " m^2";
                res.setText(resultadofinal);
                Intent intent = new Intent(MainActivity.this, activity_tab2.class);
                intent.putExtra("dos", resultadofinal);
                startActivity(intent);

            }
        });



        btngrados.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                double g = Double.valueOf(mgrados.getText().toString());
                double resultado = ((g - 32) * 5) / 9;
                String resultadofinal = resultado + " grados Celsius";
                resgrados.setText(resultadofinal);
                Intent intent = new Intent(MainActivity.this, tab1.class);
                intent.putExtra("uno", resultadofinal);
                startActivity(intent);

            }
        });


        btnmillas.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                double m = Double.valueOf(mmillas.getText().toString());
                double resultado = m * 1.609;
                String resultadofinal = resultado + " KM";
                resmillas.setText(resultadofinal);
                Intent intent = new Intent(MainActivity.this, tab3.class);
                intent.putExtra("tres", resultadofinal);
                startActivity(intent);

            }
        });




    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
