package upv.com.p4;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;


public class tab1 extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab1);


        Intent intent = getIntent();
        TextView textView = new TextView(this);
        TextView uno = (TextView) findViewById(R.id.resultadotab1);
        uno.setText(getIntent().getExtras().getString("uno") );
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tab1, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
