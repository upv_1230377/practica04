package upv.com.p4;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;


public class tab3 extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab3);

        Intent intent = getIntent();
        TextView textView = new TextView(this);
        TextView tres = (TextView) findViewById(R.id.resultadotab3);
        tres.setText(getIntent().getExtras().getString("tres"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tab3, menu);
        return super.onCreateOptionsMenu(menu);
    }


}